#!/bin/sh

# generic installs
echo "=== Attempting to compile polybar-dwm-module ==="
git clone https://github.com/mihirlad55/polybar-dwm-module
cd polybar-dwm-module || exit 1
./build.sh -d -I
cd .. || exit 1
echo "=== Polybar-dwm-module compiled successfully ==="

echo "=== Attempting to compile picom ==="
git clone https://github.com/jonaburg/picom.git
cd picom || exit 1
git submodule update --init --recursive 
meson setup --buildtype=release . build
ninja -C build
cd .. || exit 1
echo "=== Picom compiled successfully ==="

echo "=== Attempting to install pywal ==="
# shellcheck source=/dev/null
pipx install pywal && pipx ensurepath \
    && echo "export PATH=/root/.local/bin:$PATH" >> ~/.bashrc \
    && . ~/.bashrc
echo "=== Pywal installed successfully ==="

echo "=== Pywal colors setup ==="
wget https://picsum.photos/id/82/640/480 -O wpp.png
wal -i wpp.png -n
echo "=== Pywal colors setup complete ==="

echo "=== Attempting to install dwm ==="
git clone https://gitlab.com/lgcavalheiro/dwm.git
cd dwm || exit 1
make clean install
cd .. || exit 1
echo "=== Dwm installed ==="

echo "=== Attempting to install dmenu ==="
git clone https://gitlab.com/lgcavalheiro/dmenu.git
cd dmenu || exit 1
make clean install
cd .. || exit 1
echo "=== dmenu installed ==="

echo "=== Attempting to install slock ==="
git clone https://gitlab.com/lgcavalheiro/slock.git
cd slock || exit 1
make clean install
cd .. || exit 1
echo "=== slock installed ==="

echo "=== Attempting to install st ==="
git clone https://gitlab.com/lgcavalheiro/st.git
cd st || exit 1
make clean install
cd .. || exit 1
echo "=== st installed ==="
