PREFIX?=

all: check-prefix dirs configs scripts

check-prefix:
ifeq ($(PREFIX),)
	@echo "Please define a valid prefix" & exit 1
endif

dirs: 
	mkdir -p $(PREFIX)/.local/share/dwm
	mkdir -p $(PREFIX)/.config/polybar
	mkdir -p $(PREFIX)/.config/picom

configs:
	cp ./dwm/autostart.sh $(PREFIX)/.local/share/dwm
	cp ./polybar/config $(PREFIX)/.config/polybar
	cp ./picom/picom.conf $(PREFIX)/.config/picom

scripts:
	cp ./scripts/capmagick /usr/local/bin

.PHONY: all dirs configs scripts

# file possibly depracated
