#!/bin/sh

RELEASEFILE="/etc/os-release"
TZ="America/Sao_Paulo"
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

if [ "$(grep ID=arch $RELEASEFILE | wc -c)" -gt 0 ]; then
    NONROOT=""
    pacman -Syyu --needed --noconfirm git base-devel binutils fakeroot sudo

    while getopts 'u' OPT; do
        case $OPT in
        u)
            useradd non_root && mkdir /home/non_root && chown -R non_root:non_root /home/non_root
            echo "non_root ALL=NOPASSWD: ALL" >> /etc/sudoers
            NONROOT="sudo -u non_root"
            ;;
        ?) 
            die "Unexpected flag!"
            ;;
        esac
    done
    shift "$((OPTIND - 1))"
    
    eval "$NONROOT git clone https://aur.archlinux.org/yay.git"
    cd yay || exit 1
    eval "$NONROOT makepkg -si --noconfirm"
    cd .. || exit 1
    eval "xargs $NONROOT yay -S --noconfirm < ./ci/arch-pkglist"
fi

if [ "$(grep ID_LIKE=debian $RELEASEFILE | wc -c)" -gt 0 ]; then
    apt-get update && apt-get -y upgrade
    xargs apt-get -y install < ./ci/debian-pkglist
fi
