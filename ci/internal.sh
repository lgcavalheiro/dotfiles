#!/bin/sh

# === VARS ===

PREFIX=""

# === FUNCS ===

die() {
    echo "$*"
    exit 1
}

create_dirs() {
    mkdir -p "$PREFIX/.dwm"
	mkdir -p "$PREFIX/.config/polybar"
	mkdir -p "$PREFIX/.config/picom"
}

create_configs() {
    cp ./configs/autostart.sh "$PREFIX/.dwm"
	cp ./configs/config "$PREFIX/.config/polybar"
	cp ./configs/picom_jonaburg.conf "$PREFIX/.config/picom/picom.conf"
    cp -pr ./configs/wal-templates/* "$PREFIX/.config/wal/templates/"
    cp ./configs/dunstrc "$PREFIX/.config/dunst"
}

install_scripts() {
    cp ./scripts/capmgk /usr/local/bin
}

# === INIT ===

while getopts 'p:' OPT; do
	case $OPT in
	p)
        PREFIX="$OPTARG"
        ;;
    ?) 
        die "Unexpected flag!"
        ;;
	esac
done
shift "$((OPTIND - 1))"

[ -z "$PREFIX" ] && die "Please define a prefix!"

create_dirs
create_configs
install_scripts
