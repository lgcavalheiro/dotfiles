#!/bin/sh

# .dwm/autostart.sh

WALLPAPER="/path/to/wallpaper.png"

sleep 0.3
nm-applet & 
picom & 
polybar main &
wal -R -n &
feh --bg-fill "$WALLPAPER" & 

